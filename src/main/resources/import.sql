INSERT INTO public.categoria (cat_nombre) VALUES ('Daño maquinaria'), ('Perdida maquinaria'), ('Robo maquinaria');

INSERT INTO public.estado (est_nombre) VALUES ('Radicado'), ('Leida'), ('Terminada');

INSERT INTO public.organizacion (org_nombre, org_color_hex) VALUES ('Claro', '#FE2E2E'), ('Movistar', '#2676BA'), ('ETB', '#56A385');

INSERT INTO public.perfil (per_perfil) VALUES ('ROLE_ADMINISTRADOR'), ('ROLE_USUARIO');

INSERT INTO public.persona (per_nombre, per_apellido) VALUES ('Juan S.', 'Suaza'), ('Pepito', 'Perez'), ('Ramon', 'Lopez'), ('Camila', 'Fernandez');

INSERT INTO public.usuario(usu_expirado, per_codigo, usu_bloqueado, usu_cambio_clave, usu_estado, usu_password, usu_usuario) VALUES ('F', 1, 'F', 'F', 'T', '$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2', 'Admin1');
INSERT INTO public.usuario(usu_expirado, per_codigo, usu_bloqueado, usu_cambio_clave, usu_estado, usu_password, usu_usuario) VALUES ('F', 2, 'F', 'F', 'T', '$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2', 'User1');
INSERT INTO public.usuario(usu_expirado, per_codigo, usu_bloqueado, usu_cambio_clave, usu_estado, usu_password, usu_usuario) VALUES ('F', 3, 'F', 'F', 'T', '$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2', 'User2');
INSERT INTO public.usuario(usu_expirado, per_codigo, usu_bloqueado, usu_cambio_clave, usu_estado, usu_password, usu_usuario) VALUES ('F', 4, 'F', 'F', 'T', '$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2', 'User3');

INSERT INTO public.usuario_organizacion(usu_codigo, org_codigo) VALUES (1, 1), (2, 1), (3, 2), (4, 3);

INSERT INTO public.usuario_perfil(usu_codigo, per_codigo) VALUES (1, 1), (2, 2), (3, 2), (4, 2);