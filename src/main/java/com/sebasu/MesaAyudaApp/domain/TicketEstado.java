package com.sebasu.MesaAyudaApp.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ticket_estado")
public class TicketEstado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tie_codigo")
	private Long codigo;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "tic_codigo")
	@JsonBackReference
	private Ticket ticket;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "est_codigo")
	private Estado estado;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "usu_codigo")
	private Usuario usuario;

	@Column(name = "tie_comentario")
	private String comentario;

	@Column(name = "tie_fecha", columnDefinition = "timestamp")
	private Timestamp fecha;

	@Column(name = "tie_activo")
	private int activo;

	public TicketEstado() {

	}

	public TicketEstado(Long codigo, Ticket ticket, Estado estado, Usuario usuario, String comentario, Timestamp fecha,
			int activo) {
		this.codigo = codigo;
		this.ticket = ticket;
		this.estado = estado;
		this.usuario = usuario;
		this.comentario = comentario;
		this.fecha = fecha;
		this.activo = activo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	@Override
	public String toString() {
		return "TicketEstado [codigo=" + codigo + ", ticket=" + ticket + ", estado=" + estado + ", usuario=" + usuario
				+ ", comentario=" + comentario + ", fecha=" + fecha + ", activo=" + activo + "]";
	}

}
