package com.sebasu.MesaAyudaApp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "organizacion")
public class Organizacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "org_codigo")
	private Long codigo;

	@Column(name = "org_nombre")
	private String nombre;

	@Column(name = "org_color_hex")
	private String colorHex;

	@Transient
	private int totalTickets;

	public Organizacion() {

	}

	public Organizacion(Long codigo, String nombre, String colorHex) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.colorHex = colorHex;
	}

	public Organizacion(Long codigo, String nombre, String colorHex, int totalTickets) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.colorHex = colorHex;
		this.totalTickets = totalTickets;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColorHex() {
		return colorHex;
	}

	public void setColorHex(String colorHex) {
		this.colorHex = colorHex;
	}

	public int getTotalTickets() {
		return totalTickets;
	}

	public void setTotalTickets(int totalTickets) {
		this.totalTickets = totalTickets;
	}

	@Override
	public String toString() {
		return "Organizacion [codigo=" + codigo + ", nombre=" + nombre + ", colorHex=" + colorHex + ", totalTickets="
				+ totalTickets + "]";
	}

}
