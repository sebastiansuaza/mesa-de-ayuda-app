package com.sebasu.MesaAyudaApp.domain;

public class Rol {

	public final static String ADMINISTRADOR = "ROLE_ADMINISTRADOR";
	public final static String USUARIO = "ROLE_USUARIO";

}
