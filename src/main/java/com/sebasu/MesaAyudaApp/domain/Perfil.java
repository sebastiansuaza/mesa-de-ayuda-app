package com.sebasu.MesaAyudaApp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "perfil")
public class Perfil implements GrantedAuthority, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "per_codigo")
	private Long codigo;

	@Column(name = "per_perfil", nullable = false)
	private String perfil;

	public Perfil() {

	}

	public Perfil(Long codigo, String perfil) {
		this.codigo = codigo;
		this.perfil = perfil;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getPerfil() {
		return perfil;
	}

	@Override
	public String getAuthority() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	@Override
	public String toString() {
		return "Perfil [codigo=" + codigo + ", perfil=" + perfil + "]";
	}

}
