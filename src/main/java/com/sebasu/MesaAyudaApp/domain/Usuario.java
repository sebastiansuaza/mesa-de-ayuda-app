package com.sebasu.MesaAyudaApp.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "usu_codigo")
	private Long codigo;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "per_codigo")
	private Persona persona;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "usuario")
	private UsuarioOrganizacion organizacion;

	@Column(name = "usu_usuario", nullable = false)
	private String usuario;

	@Column(name = "usu_password", nullable = false)
	private String password;

	@Column(name = "usu_estado", nullable = false)
	private boolean enabled;

	@Column(name = "usu_expirado", nullable = false)
	private boolean accountNonExpired;

	@Column(name = "usu_cambio_clave", nullable = false)
	private boolean credentialNonExpired;

	@Column(name = "usu_bloqueado", nullable = false)
	private boolean accountNonLocked;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "usuario")
	private List<UsuarioPerfil> perfiles;

	public Usuario() {

	}

	public Usuario(Long codigo, Persona persona, UsuarioOrganizacion organizacion, String usuario, String password,
			boolean enabled, boolean accountNonExpired, boolean credentialNonExpired, boolean accountNonLocked,
			List<UsuarioPerfil> perfiles) {
		this.codigo = codigo;
		this.persona = persona;
		this.organizacion = organizacion;
		this.usuario = usuario;
		this.password = password;
		this.enabled = enabled;
		this.accountNonExpired = accountNonExpired;
		this.credentialNonExpired = credentialNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.perfiles = perfiles;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public UsuarioOrganizacion getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(UsuarioOrganizacion organizacion) {
		this.organizacion = organizacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isCredentialNonExpired() {
		return credentialNonExpired;
	}

	public void setCredentialNonExpired(boolean credentialNonExpired) {
		this.credentialNonExpired = credentialNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public List<UsuarioPerfil> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(List<UsuarioPerfil> perfiles) {
		for (UsuarioPerfil usuarioPerfil : perfiles) {
			usuarioPerfil.setUsuario(this);
		}
		this.perfiles = perfiles;
	}

	@Override
	public String toString() {
		return "Usuario [codigo=" + codigo + ", persona=" + persona + ", organizacion=" + organizacion + ", usuario="
				+ usuario + ", password=" + password + ", enabled=" + enabled + ", accountNonExpired="
				+ accountNonExpired + ", credentialNonExpired=" + credentialNonExpired + ", accountNonLocked="
				+ accountNonLocked + ", perfiles=" + perfiles + "]";
	}

}
