package com.sebasu.MesaAyudaApp.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class Ticket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tic_codigo")
	private Long codigo;

	@Column(name = "tic_descripcion", nullable = false)
	private String descripcion;

	@Column(name = "tic_persona", nullable = false)
	private String persona;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "cat_codigo")
	private Categoria categoria;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "org_codigo")
	private Organizacion organizacion;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "usu_codigo")
	private Usuario usuario;

	@Column(name = "tie_fecha", columnDefinition = "timestamp")
	private Timestamp fecha;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "ticket")
	private List<TicketEstado> estados;

	public Ticket() {

	}

	public Ticket(Long codigo, String descripcion, String persona, Categoria categoria, Organizacion organizacion,
			Usuario usuario, Timestamp fecha, List<TicketEstado> estados) {
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.persona = persona;
		this.categoria = categoria;
		this.organizacion = organizacion;
		this.usuario = usuario;
		this.fecha = fecha;
		this.estados = estados;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Organizacion getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public List<TicketEstado> getEstados() {
		return estados;
	}

	public void setEstados(List<TicketEstado> estados) {
		this.estados = estados;
	}

	@Override
	public String toString() {
		return "Ticket [codigo=" + codigo + ", descripcion=" + descripcion + ", persona=" + persona + ", categoria="
				+ categoria + ", organizacion=" + organizacion + ", usuario=" + usuario + ", fecha=" + fecha
				+ ", estados=" + estados + "]";
	}

}
