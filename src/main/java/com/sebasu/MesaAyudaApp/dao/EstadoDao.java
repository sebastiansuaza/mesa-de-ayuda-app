package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.Estado;

@Repository
public interface EstadoDao extends JpaRepository<Estado, Long> {

}
