package com.sebasu.MesaAyudaApp.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.Ticket;

@Repository
public interface TicketDao extends JpaRepository<Ticket, Long> {

	public List<Ticket> findAllByOrganizacionCodigo(Long organizacion);

	public List<Ticket> findAllByUsuarioCodigo(Long usuario);

	public Page<Ticket> findAllByOrganizacionCodigo(Long organizacion, Pageable pageable);

	public Page<Ticket> findAllByUsuarioCodigo(Long usuario, Pageable pageable);

	public long countByUsuarioCodigo(Long usuario);

	public long countByOrganizacionCodigo(Long organizacion);

}
