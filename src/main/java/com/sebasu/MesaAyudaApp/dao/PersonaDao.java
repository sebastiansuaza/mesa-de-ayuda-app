package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.Persona;

@Repository
public interface PersonaDao extends JpaRepository<Persona, Long> {

}
