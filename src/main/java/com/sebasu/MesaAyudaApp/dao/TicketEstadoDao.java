package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.TicketEstado;

@Repository
public interface TicketEstadoDao extends JpaRepository<TicketEstado, Long> {

}
