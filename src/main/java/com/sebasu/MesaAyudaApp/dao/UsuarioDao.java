package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sebasu.MesaAyudaApp.domain.Usuario;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Long> {

	public Usuario findByUsuario(String usuario);

}
