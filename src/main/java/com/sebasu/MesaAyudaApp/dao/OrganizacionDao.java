package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.Organizacion;

@Repository
public interface OrganizacionDao extends JpaRepository<Organizacion, Long> {

}
