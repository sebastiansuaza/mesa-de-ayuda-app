package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.Perfil;

@Repository
public interface PerfilDao extends JpaRepository<Perfil, Long> {

}
