package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.UsuarioOrganizacion;

@Repository
public interface UsuarioOrganizacionDao extends JpaRepository<UsuarioOrganizacion, Long> {

}
