package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.UsuarioPerfil;

@Repository
public interface UsuarioPerfilDao extends JpaRepository<UsuarioPerfil, Long> {

}
