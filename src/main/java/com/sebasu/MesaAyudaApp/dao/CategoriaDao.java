package com.sebasu.MesaAyudaApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyudaApp.domain.Categoria;

@Repository
public interface CategoriaDao extends JpaRepository<Categoria, Long> {

}
