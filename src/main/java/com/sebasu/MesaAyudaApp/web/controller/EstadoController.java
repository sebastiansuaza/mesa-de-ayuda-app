package com.sebasu.MesaAyudaApp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sebasu.MesaAyudaApp.domain.Estado;
import com.sebasu.MesaAyudaApp.service.EstadoService;

@RestController
@RequestMapping("api/estado")
public class EstadoController {

	@Autowired
	EstadoService estadoService;

	@GetMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public List<Estado> listar() {
		return estadoService.listar();
	}
	
	@GetMapping(value = "total")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public long total() {
		return estadoService.total();
	}

}
