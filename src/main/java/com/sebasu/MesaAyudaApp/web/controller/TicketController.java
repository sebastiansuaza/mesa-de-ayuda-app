package com.sebasu.MesaAyudaApp.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sebasu.MesaAyudaApp.domain.Rol;
import com.sebasu.MesaAyudaApp.domain.Ticket;
import com.sebasu.MesaAyudaApp.domain.Usuario;
import com.sebasu.MesaAyudaApp.service.TicketService;
import com.sebasu.MesaAyudaApp.service.UsuarioService;

@RestController
@RequestMapping("api/ticket")
public class TicketController {

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	TicketService ticketService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<Ticket> ticket = ticketService.buscarPorId(codigo);
		return ticket.isPresent() ? new ResponseEntity<Object>(ticket, HttpStatus.OK)
				: new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "total")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public long total(Authentication authentication) {
		if (authentication.getAuthorities().toString().contains(Rol.ADMINISTRADOR)) {
			return ticketService.total();
		} else {
			Usuario usuario = usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
			return ticketService.totalPorOrganizacion(usuario.getOrganizacion().getOrganizacion().getCodigo());
		}

	}

	@GetMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public List<Ticket> listar(Authentication authentication) {
		if (authentication.getAuthorities().toString().contains(Rol.ADMINISTRADOR)) {
			return ticketService.listar();
		} else {
			Usuario usuario = usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
			return ticketService.buscarPorOrganizacion(usuario.getOrganizacion().getOrganizacion().getCodigo());
		}
	}

	@GetMapping(value = "usuario")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public List<Ticket> listarPorUsuario(Authentication authentication) {
		Usuario usuario = usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
		return ticketService.buscarPorOrganizacion(usuario.getCodigo());
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public Page<Ticket> listarPaginado(Authentication authentication,
			@PageableDefault(size = 5) @SortDefault.SortDefaults({
					@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		if (authentication.getAuthorities().toString().contains(Rol.ADMINISTRADOR)) {
			return ticketService.listar(pageable);
		} else {
			Usuario usuario = usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
			return ticketService.buscarPorUsuario(usuario.getCodigo(), pageable);
		}
	}

	@GetMapping(value = "usuario/paginacion")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public Page<Ticket> listarPaginadoPorUsuario(Authentication authentication,
			@PageableDefault(size = 5) @SortDefault.SortDefaults({
					@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		if (authentication.getAuthorities().toString().contains(Rol.ADMINISTRADOR)) {
			return ticketService.listar(pageable);
		} else {
			Usuario usuario = usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
			return ticketService.buscarPorUsuario(usuario.getCodigo(), pageable);
		}
	}

	@GetMapping(value = "organizacion/paginacion")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public Page<Ticket> listarPaginadoPorOrganizacion(Authentication authentication,
			@PageableDefault(size = 5) @SortDefault.SortDefaults({
					@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		if (authentication.getAuthorities().toString().contains(Rol.ADMINISTRADOR)) {
			return ticketService.listar(pageable);
		} else {
			Usuario usuario = usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
			return ticketService.buscarPorOrganizacion(usuario.getOrganizacion().getOrganizacion().getCodigo(),
					pageable);
		}
	}

	@PostMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public ResponseEntity<Object> agregar(@Valid @RequestBody Ticket ticket) {
		return new ResponseEntity<Object>(ticketService.agregar(ticket), HttpStatus.CREATED);
	}

	@PutMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public ResponseEntity<Object> modificar(@Valid @RequestBody Ticket ticket) {
		return new ResponseEntity<Object>(ticketService.modificar(ticket), HttpStatus.OK);
	}

}
