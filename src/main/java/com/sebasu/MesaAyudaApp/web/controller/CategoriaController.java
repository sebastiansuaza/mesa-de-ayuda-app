package com.sebasu.MesaAyudaApp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sebasu.MesaAyudaApp.domain.Categoria;
import com.sebasu.MesaAyudaApp.service.CategoriaService;

@RestController
@RequestMapping("api/categoria")
public class CategoriaController {

	@Autowired
	CategoriaService categoriaService;

	@GetMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public List<Categoria> listar() {
		return categoriaService.listar();
	}
	
	@GetMapping(value = "total")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public long total() {
		return categoriaService.total();
	}

}
