package com.sebasu.MesaAyudaApp.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sebasu.MesaAyudaApp.domain.Usuario;
import com.sebasu.MesaAyudaApp.service.UsuarioService;

@RestController
@RequestMapping("api/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;

	@GetMapping(value = "user")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public Usuario buscarPorId(Authentication authentication) {
		return usuarioService.buscarPorUsuario(authentication.getPrincipal().toString());
	}

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<Usuario> paciente = usuarioService.buscarPorId(codigo);
		return paciente.isPresent() ? new ResponseEntity<Object>(paciente, HttpStatus.OK)
				: new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "total")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public long total() {
		return usuarioService.total();
	}

	@GetMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public List<Usuario> listar() {
		return usuarioService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public Page<Usuario> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return usuarioService.listar(pageable);
	}

	@PostMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public ResponseEntity<Object> agregar(@Valid @RequestBody Usuario usuario) {
		return new ResponseEntity<Object>(usuarioService.agregar(usuario), HttpStatus.CREATED);
	}

	@PutMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public ResponseEntity<Object> modificar(@Valid @RequestBody Usuario usuario) {
		return new ResponseEntity<Object>(usuarioService.modificar(usuario), HttpStatus.OK);
	}

}
