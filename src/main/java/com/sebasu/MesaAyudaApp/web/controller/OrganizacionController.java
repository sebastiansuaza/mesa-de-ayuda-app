package com.sebasu.MesaAyudaApp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sebasu.MesaAyudaApp.domain.Organizacion;
import com.sebasu.MesaAyudaApp.service.OrganizacionService;

@RestController
@RequestMapping("api/organizacion")
public class OrganizacionController {

	@Autowired
	OrganizacionService organizacionService;

	@GetMapping
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public List<Organizacion> listar() {
		return organizacionService.listar();
	}
	
	@GetMapping(value = "total")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR') or hasRole('ROLE_USUARIO')")
	public long total() {
		return organizacionService.total();
	}

}
