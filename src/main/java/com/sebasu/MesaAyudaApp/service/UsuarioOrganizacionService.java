package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.UsuarioOrganizacion;

public interface UsuarioOrganizacionService {

	public UsuarioOrganizacion agregar(UsuarioOrganizacion organizacion);

	public UsuarioOrganizacion modificar(UsuarioOrganizacion organizacion);

}
