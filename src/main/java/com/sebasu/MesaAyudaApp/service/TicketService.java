package com.sebasu.MesaAyudaApp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.sebasu.MesaAyudaApp.domain.Ticket;
import com.sebasu.MesaAyudaApp.service.util.CrudServiceInterface;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;
import com.sebasu.MesaAyudaApp.service.util.PaginationServiceInterface;

public interface TicketService extends CrudServiceInterface<Ticket, Long>, ListServiceInterface<Ticket, Long>,
		PaginationServiceInterface<Ticket, Long> {

	public List<Ticket> buscarPorOrganizacion(Long usuario);

	public List<Ticket> buscarPorUsuario(Long organizacion);

	public Page<Ticket> buscarPorOrganizacion(Long organizacion, Pageable pageable);

	public Page<Ticket> buscarPorUsuario(Long usuario, Pageable pageable);

	public long totalPorUsuario(Long usuario);

	public long totalPorOrganizacion(Long organizacion);

}
