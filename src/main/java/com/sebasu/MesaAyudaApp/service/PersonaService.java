package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.Persona;
import com.sebasu.MesaAyudaApp.service.util.CrudServiceInterface;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;
import com.sebasu.MesaAyudaApp.service.util.PaginationServiceInterface;

public interface PersonaService extends CrudServiceInterface<Persona, Long>, ListServiceInterface<Persona, Long>,
		PaginationServiceInterface<Persona, Long> {

}
