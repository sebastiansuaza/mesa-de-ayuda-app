package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.Organizacion;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;

public interface OrganizacionService extends ListServiceInterface<Organizacion, Long> {

}
