package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.Perfil;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;

public interface PerfilService extends ListServiceInterface<Perfil, Long> {

}
