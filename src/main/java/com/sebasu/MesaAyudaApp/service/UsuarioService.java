package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.Usuario;
import com.sebasu.MesaAyudaApp.service.util.CrudServiceInterface;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;
import com.sebasu.MesaAyudaApp.service.util.PaginationServiceInterface;

public interface UsuarioService extends CrudServiceInterface<Usuario, Long>, ListServiceInterface<Usuario, Long>,
		PaginationServiceInterface<Usuario, Long> {

	public Usuario buscarPorUsuario(String usuario);

}
