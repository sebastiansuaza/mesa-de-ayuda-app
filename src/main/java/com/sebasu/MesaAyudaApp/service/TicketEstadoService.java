package com.sebasu.MesaAyudaApp.service;

import java.util.List;

import com.sebasu.MesaAyudaApp.domain.TicketEstado;
import com.sebasu.MesaAyudaApp.service.util.CrudServiceInterface;

public interface TicketEstadoService extends CrudServiceInterface<TicketEstado, Long> {

	public List<TicketEstado> agregar(List<TicketEstado> t);
	
	public List<TicketEstado> modificar(List<TicketEstado> t);
	
}
