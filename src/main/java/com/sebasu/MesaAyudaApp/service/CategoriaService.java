package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.Categoria;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;

public interface CategoriaService extends ListServiceInterface<Categoria, Long> {

}
