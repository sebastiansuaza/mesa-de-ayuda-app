package com.sebasu.MesaAyudaApp.service;

import java.util.List;

import com.sebasu.MesaAyudaApp.domain.UsuarioPerfil;

public interface UsuarioPerfilService {

	public List<UsuarioPerfil> agregar(List<UsuarioPerfil> perfiles);
	
	public List<UsuarioPerfil> modificar(List<UsuarioPerfil> perfiles);
	
}
