package com.sebasu.MesaAyudaApp.service.util;

import java.util.Optional;

public interface CrudServiceInterface<T, ID> {
	
	public Optional<T> buscarPorId(ID id);

	public T agregar(T t);

	public T modificar(T t);

}
