package com.sebasu.MesaAyudaApp.service.util;

import java.util.List;

public interface ListServiceInterface<T, ID> {

	public List<T> listar();
	
	public long total();

}
