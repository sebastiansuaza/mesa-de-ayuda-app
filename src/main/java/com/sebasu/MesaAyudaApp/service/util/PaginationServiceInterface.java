package com.sebasu.MesaAyudaApp.service.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaginationServiceInterface<T, ID> {

	public Page<T> listar(Pageable pageable);

}
