package com.sebasu.MesaAyudaApp.service;

import com.sebasu.MesaAyudaApp.domain.Estado;
import com.sebasu.MesaAyudaApp.service.util.ListServiceInterface;

public interface EstadoService extends ListServiceInterface<Estado, Long> {

}
