package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.EstadoDao;
import com.sebasu.MesaAyudaApp.domain.Estado;
import com.sebasu.MesaAyudaApp.service.EstadoService;

@Service
public class EstadoServiceImpl implements EstadoService {

	@Autowired
	EstadoDao estadoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Estado> listar() {
		return estadoDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public long total() {
		return estadoDao.count();
	}

}
