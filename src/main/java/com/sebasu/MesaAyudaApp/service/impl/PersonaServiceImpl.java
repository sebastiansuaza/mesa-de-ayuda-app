package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.PersonaDao;
import com.sebasu.MesaAyudaApp.domain.Persona;
import com.sebasu.MesaAyudaApp.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaDao personaDao;

	@Override
	@Transactional(readOnly = true)
	public Optional<Persona> buscarPorId(Long id) {
		return personaDao.findById(id);
	}

	@Override
	@Transactional
	public Persona agregar(Persona t) {
		return personaDao.save(t);
	}

	@Override
	@Transactional
	public Persona modificar(Persona t) {
		return personaDao.save(t);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Persona> listar() {
		return personaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Persona> listar(Pageable pageable) {
		return personaDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public long total() {
		return personaDao.count();
	}

}
