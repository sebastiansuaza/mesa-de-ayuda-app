package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.UsuarioDao;
import com.sebasu.MesaAyudaApp.domain.Usuario;
import com.sebasu.MesaAyudaApp.domain.UsuarioPerfil;
import com.sebasu.MesaAyudaApp.service.PersonaService;
import com.sebasu.MesaAyudaApp.service.UsuarioOrganizacionService;
import com.sebasu.MesaAyudaApp.service.UsuarioPerfilService;
import com.sebasu.MesaAyudaApp.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioDao usuarioDao;

	@Autowired
	PersonaService personaService;

	@Autowired
	UsuarioPerfilService usuarioPerfilService;

	@Autowired
	UsuarioOrganizacionService usuarioOrganizacionService;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	@Transactional(readOnly = true)
	public Optional<Usuario> buscarPorId(Long id) {
		return usuarioDao.findById(id);
	}

	@Override
	@Transactional
	public Usuario agregar(Usuario t) {
		t.setPassword(passwordEncoder.encode(t.getPassword()));
		t.setPersona(personaService.agregar(t.getPersona()));
		Usuario usuario = usuarioDao.save(t);
		for (UsuarioPerfil usuarioPerfil : t.getPerfiles()) {
			usuarioPerfil.setUsuario(usuario);
		}
		usuarioPerfilService.agregar(t.getPerfiles());

		if (t.getOrganizacion() != null) {
			t.getOrganizacion().setUsuario(usuario);
		}
		usuarioOrganizacionService.agregar(t.getOrganizacion());
		return usuario;
	}

	@Override
	@Transactional
	public Usuario modificar(Usuario t) {
		t.setPersona(personaService.modificar(t.getPersona()));
		Usuario usuario = usuarioDao.save(t);
		for (UsuarioPerfil usuarioPerfil : t.getPerfiles()) {
			usuarioPerfil.setUsuario(usuario);
		}
		usuarioPerfilService.modificar(t.getPerfiles());
		return usuario;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> listar() {
		return usuarioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Usuario> listar(Pageable pageable) {
		return usuarioDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario buscarPorUsuario(String usuario) {
		return usuarioDao.findByUsuario(usuario);
	}

	@Override
	@Transactional(readOnly = true)
	public long total() {
		return usuarioDao.count();
	}

}
