package com.sebasu.MesaAyudaApp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.UsuarioOrganizacionDao;
import com.sebasu.MesaAyudaApp.domain.UsuarioOrganizacion;
import com.sebasu.MesaAyudaApp.service.UsuarioOrganizacionService;

@Service
public class UsuarioOrganizacionServiceImpl implements UsuarioOrganizacionService {

	@Autowired
	UsuarioOrganizacionDao usuarioOrganizacionDao;

	@Override
	@Transactional
	public UsuarioOrganizacion agregar(UsuarioOrganizacion organizacion) {
		return usuarioOrganizacionDao.save(organizacion);
	}

	@Override
	@Transactional
	public UsuarioOrganizacion modificar(UsuarioOrganizacion organizacion) {
		return usuarioOrganizacionDao.save(organizacion);
	}

}
