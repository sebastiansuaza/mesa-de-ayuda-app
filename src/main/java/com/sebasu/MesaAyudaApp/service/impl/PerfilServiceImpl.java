package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.PerfilDao;
import com.sebasu.MesaAyudaApp.domain.Perfil;
import com.sebasu.MesaAyudaApp.service.PerfilService;

@Service
public class PerfilServiceImpl implements PerfilService {

	@Autowired
	PerfilDao perfilDao;

	@Override
	@Transactional(readOnly = true)
	public List<Perfil> listar() {
		return perfilDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public long total() {
		return perfilDao.count();
	}

}
