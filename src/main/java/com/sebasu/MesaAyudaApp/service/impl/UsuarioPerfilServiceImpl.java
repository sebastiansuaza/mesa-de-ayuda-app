package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.UsuarioPerfilDao;
import com.sebasu.MesaAyudaApp.domain.UsuarioPerfil;
import com.sebasu.MesaAyudaApp.service.UsuarioPerfilService;

@Service
public class UsuarioPerfilServiceImpl implements UsuarioPerfilService {

	@Autowired
	UsuarioPerfilDao usuarioPerfilDao;

	@Override
	@Transactional
	public List<UsuarioPerfil> agregar(List<UsuarioPerfil> perfiles) {
		return usuarioPerfilDao.saveAll(perfiles);
	}

	@Override
	@Transactional
	public List<UsuarioPerfil> modificar(List<UsuarioPerfil> perfiles) {
		return usuarioPerfilDao.saveAll(perfiles);
	}

}
