package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.OrganizacionDao;
import com.sebasu.MesaAyudaApp.domain.Organizacion;
import com.sebasu.MesaAyudaApp.service.OrganizacionService;
import com.sebasu.MesaAyudaApp.service.TicketService;

@Service
public class OrganizacionServiceImpl implements OrganizacionService {

	@Autowired
	OrganizacionDao organizacionDao;

	@Autowired
	TicketService ticketService;

	@Override
	@Transactional(readOnly = true)
	public List<Organizacion> listar() {
		List<Organizacion> list = organizacionDao.findAll();
		for (Organizacion organizacion : list) {
			organizacion.setTotalTickets(ticketService.buscarPorOrganizacion(organizacion.getCodigo()).size());
		}
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public long total() {
		return organizacionDao.count();
	}

}
