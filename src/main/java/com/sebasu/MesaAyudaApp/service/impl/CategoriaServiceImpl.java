package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.CategoriaDao;
import com.sebasu.MesaAyudaApp.domain.Categoria;
import com.sebasu.MesaAyudaApp.service.CategoriaService;

@Service
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	CategoriaDao categoriaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Categoria> listar() {
		return categoriaDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public long total() {
		return categoriaDao.count();
	}

}
