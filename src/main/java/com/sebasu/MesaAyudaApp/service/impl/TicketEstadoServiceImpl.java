package com.sebasu.MesaAyudaApp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.TicketEstadoDao;
import com.sebasu.MesaAyudaApp.domain.TicketEstado;
import com.sebasu.MesaAyudaApp.service.TicketEstadoService;

@Service
public class TicketEstadoServiceImpl implements TicketEstadoService {

	@Autowired
	TicketEstadoDao ticketEstadoDao;

	@Override
	@Transactional
	public TicketEstado agregar(TicketEstado t) {
		return ticketEstadoDao.save(t);
	}

	@Override
	@Transactional
	public TicketEstado modificar(TicketEstado t) {
		return ticketEstadoDao.save(t);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<TicketEstado> buscarPorId(Long id) {
		return ticketEstadoDao.findById(id);
	}

	@Override
	@Transactional
	public List<TicketEstado> agregar(List<TicketEstado> t) {
		return ticketEstadoDao.saveAll(t);
	}

	@Override
	@Transactional
	public List<TicketEstado> modificar(List<TicketEstado> t) {
		return ticketEstadoDao.saveAll(t);
	}

}
