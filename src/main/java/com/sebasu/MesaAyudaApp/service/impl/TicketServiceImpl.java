package com.sebasu.MesaAyudaApp.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyudaApp.dao.TicketDao;
import com.sebasu.MesaAyudaApp.domain.Ticket;
import com.sebasu.MesaAyudaApp.domain.TicketEstado;
import com.sebasu.MesaAyudaApp.service.TicketEstadoService;
import com.sebasu.MesaAyudaApp.service.TicketService;

@Service
public class TicketServiceImpl implements TicketService {

	@Autowired
	TicketDao ticketDao;

	@Autowired
	TicketEstadoService ticketEstadoService;

	@Override
	@Transactional(readOnly = true)
	public Optional<Ticket> buscarPorId(Long id) {
		return ticketDao.findById(id);
	}

	@Override
	@Transactional
	public Ticket agregar(Ticket t) {
		t.setFecha(new Timestamp(new Date().getTime()));
		Ticket ticket = ticketDao.save(t);
		for (TicketEstado ticketEstado : t.getEstados()) {
			ticketEstado.setTicket(ticket);
			ticketEstado.setFecha(new Timestamp(new Date().getTime()));
		}
		ticketEstadoService.agregar(t.getEstados());
		return ticket;
	}

	@Override
	@Transactional
	public Ticket modificar(Ticket t) {
		Ticket ticket = ticketDao.save(t);
		for (TicketEstado ticketEstado : t.getEstados()) {
			ticketEstado.setTicket(ticket);
			if (ticketEstado.getFecha() == null) {
				ticketEstado.setFecha(new Timestamp(new Date().getTime()));
			}
		}
		ticketEstadoService.modificar(t.getEstados());
		return ticket;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Ticket> listar() {
		return ticketDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Ticket> listar(Pageable pageable) {
		return ticketDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Ticket> buscarPorOrganizacion(Long organizacion) {
		return ticketDao.findAllByOrganizacionCodigo(organizacion);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Ticket> buscarPorOrganizacion(Long organizacion, Pageable pageable) {
		return ticketDao.findAllByOrganizacionCodigo(organizacion, pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Ticket> buscarPorUsuario(Long usuario) {
		return ticketDao.findAllByUsuarioCodigo(usuario);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Ticket> buscarPorUsuario(Long usuario, Pageable pageable) {
		return ticketDao.findAllByUsuarioCodigo(usuario, pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public long total() {
		return ticketDao.count();
	}

	@Override
	@Transactional(readOnly = true)
	public long totalPorUsuario(Long usuario) {
		return ticketDao.countByUsuarioCodigo(usuario);
	}

	@Override
	@Transactional(readOnly = true)
	public long totalPorOrganizacion(Long organizacion) {
		return ticketDao.countByOrganizacionCodigo(organizacion);
	}

}
