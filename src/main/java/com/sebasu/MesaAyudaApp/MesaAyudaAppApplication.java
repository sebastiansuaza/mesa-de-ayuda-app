package com.sebasu.MesaAyudaApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

//@EnableOAuth2Sso
@EnableJpaAuditing
@EnableJpaRepositories
@EnableWebMvc
@SpringBootApplication
public class MesaAyudaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MesaAyudaAppApplication.class, args);
	}

}
